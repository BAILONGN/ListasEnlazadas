package ListasEnlazadas;

import javax.swing.JOptionPane;


/*
    LISTA SIMPLEMENTE ENLAZADA DE DATOS ENTEROS
*/
class CNodo {
    
	int dato;
	CNodo siguiente;
        
	public CNodo()	{
            siguiente = null;
	}
}

class CLista {
    
    CNodo cabeza;
    
    public CLista()
    {
            cabeza = null;
    }
    
    public void InsertarDato(int dat) {
        if(dat<0){
            
        
        CNodo NuevoNodo;
        CNodo antes, luego;
        NuevoNodo = new CNodo();
        NuevoNodo.dato=dat;
        int ban=0;
        if (cabeza == null){ //lista esta vacia
            NuevoNodo.siguiente=null;
            cabeza = NuevoNodo;
        }
        else {
            if (dat<cabeza.dato) //dato va antes de cabeza
            {
                    NuevoNodo.siguiente=cabeza;
                    cabeza = NuevoNodo;
            }
                else {  
                        antes=cabeza;
                        luego=cabeza;
                        while (ban==0)
                        {
                            if (dat>=luego.dato) 
                            {
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            if (luego==null)
                            {
                                ban=1;
                            }
                            else 
                            {
                                    if (dat<luego.dato){
                                        ban=1;
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
        }
    }
        else{
            System.out.println("Solo se permiten enteros negativos:");
        }
    }

    public void EliminarDato(int dat) {
        CNodo antes,luego;
        int ban=0;
        if (Vacia()) {
            System.out.print("Lista vacía ");
        }
        else {  if (dat<cabeza.dato) {
                    System.out.print("dato no existe en la lista ");
                }
                else {
                        if (dat==cabeza.dato) {
                            cabeza=cabeza.siguiente;
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) {
                                    if (dat>luego.dato) {
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    if (luego==null) {
                                        ban=1;
                                    }
                                    else {
                                            if (luego.dato==dat) 
                                                ban=1;
                                    }
                                }
                                if (luego==null) {
                                    System.out.print("dato no existe en la Lista ");
                                }
                                else {
                                        if (dat==luego.dato) {
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                            System.out.print("dato no existe en la Lista ");
                                }
                        }
                }
        }
    }

    public boolean Vacia() {
        return(cabeza==null);
    }

    public void Imprimir() {
        CNodo Temporal;
        Temporal=cabeza;
        if (!Vacia()) {
            while (Temporal!=null) {
                System.out.print(" " + Temporal.dato +" ");
                Temporal = Temporal.siguiente;
            }
            System.out.println("");
        }
        else
            System.out.print("Lista vacía");
    }
}

public class ListasEnlazadas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
       
            
        CLista lista = new CLista();
       
        do {
            
                
            dato = Integer.parseInt(JOptionPane.showInputDialog("menu\n1.ingresar dato a la lista\n" + "2.mostrar datos de la lista\n" + "3.eliminar dato\n" + "4.salir"));
            switch (dato) {
                case 1:
                   dato=Integer.parseInt(JOptionPane.showInputDialog(null,"ingresar elemento"));
                    lista.InsertarDato(dato);
                    break;
                case 2:
                    lista.Imprimir();
                    break;
                case 3:
                    dato=Integer.parseInt(JOptionPane.showInputDialog(null,"ingresar elemento a eliminar"));
                    lista.EliminarDato(dato);
            }
           
                
                 
        } while (dato != 4);
                }
}
    

